��    %      D  5   l      @  	   A  	   K  
   U     `     f     s     y     �     �  +   �     �     �       c        �     �  	   �     �     �     �     �     �     �  #   �  "     ,   4     a     g  )   v     �     �     �     �                 �  (  	     	             %  	   ,     6     <      W     x  1   �     �     �     �  B   �     ;	     B	  	   S	     ]	     f	     s	     {	  
   �	     �	     �	     �	  ,   �	     
  !   
  3   <
     p
     �
     �
     �
     �
     �
     �
                                        	   
                                              !                                 %                           "      $                    #    Add Group Add group Add groups Apply Choose group Close Completely Remove group Completely Remove group? Enter groupname First and second password must be identical Group Password No group Recoverable: No group Selected: Notice!
        A group can only be recovered if the group
        has not been completely removed! Password Password Management Passwords Recover Recover group Remove Remove group Repair group Select Group The groupname cannot contain spaces The password cannot contain spaces There are no records of a recoverable group. Users Users to Group You MUST be root to use this application! You must choose a group You need to enter a groupname You need to enter a password group Password group Removal password password again Project-Id-Version: antix-development
POT-Creation-Date: 2013-07-06 17:18+EEST
PO-Revision-Date: 2018-09-23 22:14+0300
Last-Translator: mahmut özcan <mahmutozcan65@yahoo.com>
Language-Team: Turkish (http://www.transifex.com/anticapitalista/antix-development/language/tr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: tr
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 1.8.11
X-Poedit-SourceCharset: UTF-8
 Grup Ekle Grup ekle Gruplar ekle Uygula Grup seç Kapat Grup Tamamen Kaldırıldı Grup Tamamen Kaldırılsın mı? Grup adı girin Birinci ve ikinci parola birbiriyle aynı olmalı Grup Parolası Kurtarılabilir grup yok: Seçilmiş grup yok: Duyuru
Tamamen kurtarılabilir bir grupsa
tamamen kaldırılmadı! Parola Parola Yönetimi Parolalar Kurtarma Grubu kurtar Kaldır Grubu kaldır Grubu onar Grup Seçimi Grup adı boşluklar içeremez Parola boşluklar içermemeli Kurtarılabilir bir gruba ait kayıt yoktur. Kullanıcılar Gruplandırılacak Kullanıcılar Bu uygulamayı kullanmak için root olmalısınız! Bir grup seçmelisiniz Bir grup adı girmelisiniz Bir parola girmeniz gerekli Grup Parolası grup kaldırma parola Parola tekrarı 