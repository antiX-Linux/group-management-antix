��    &      L  5   |      P  	   Q  	   [  
   e     p     v     �     �     �     �  +   �     �            c   .  t   �            	   $     .     6     D     K     X     e  #   r  "   �  ,   �     �     �  )   �     %     =     [     x     �     �     �  �  �     �     �     �     �     �     �     �     	     	     4	     L	     Z	     x	  _   �	  �   �	     �
     �
     �
     �
     �
  	   �
     �
     �
     �
  &        ,  .   J     y     �  1   �     �     �     	          +     @     F           !                           	   
                                               "                                 &                           #      %                    $    Add Group Add group Add groups Apply Choose group Close Completely Remove group Completely Remove group? Enter groupname First and second password must be identical Group Password No group Recoverable: No group Selected: Notice!
        A group can only be recovered if the group
        has not been completely removed! Notice!
        All of the users in this group will be 
        logged, unless completely remove
        is checked. Password Password Management Passwords Recover Recover group Remove Remove group Repair group Select Group The groupname cannot contain spaces The password cannot contain spaces There are no records of a recoverable group. Users Users to Group You MUST be root to use this application! You must choose a group You need to enter a groupname You need to enter a password group Password group Removal password password again Project-Id-Version: antix-development
POT-Creation-Date: 2013-07-06 17:18+EEST
PO-Revision-Date: 2018-09-23 22:08+0300
Last-Translator: joyinko <joyinko@azet.sk>
Language-Team: Czech (http://www.transifex.com/anticapitalista/antix-development/language/cs/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cs
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Poedit 1.8.11
X-Poedit-SourceCharset: UTF-8
 Přidat skupinu Přidat skupinu Přidat skupiny Použít Zvolte skupinu Zavřít Skupina kompletně smazána Kompletně smazat skupinu? Vložte název skupiny Hesla se musí shodovat Heslo skupiny Žádná skupina k obnovení: Není vybrána žádná skupina Upozornění!
        Skupina může být obnovena pouze
        ak nebyla kompletně smazána! Upozornění!
        Všechny domovské složky uživatelů v této 
        skupině budou zachovány, pokud nevyberete 
        kompletní smazaní.
        Heslo Správa hesel Hesla Obnovit Obnovit skupinu Odstranit Odstranit skupinu Opravit skupinu Vybrat skupinu Název skupiny nesmí obsahovat mezery Heslo nesmí obsahovat mezery Záznam o obnovitelné skupině nebyl nalezen. Uživatelé Přidat uživatele do skupiny Musíte být root pro spuštění této aplikace! Musíte vybrat skupinu Musíte zadat název skupiny Musíte zadat heslo Heslo skupiny Odstranění skupiny Heslo Heslo znovu 