��    &      L  5   |      P  	   Q  	   [  
   e     p     v     �     �     �     �  +   �     �            c   .  t   �            	   $     .     6     D     K     X     e  #   r  "   �  ,   �     �     �  )   �     %     =     [     x     �     �     �  �  �     �     �     �     �     �     �     �     �     	  <   2	     o	     ~	     �	  S   �	  s   
     |
     �
     �
  	   �
     �
     �
     �
     �
     �
  '   �
  !     0   6     g     t  /   �     �  !   �     �          $     7     =           !                           	   
                                               "                                 &                           #      %                    $    Add Group Add group Add groups Apply Choose group Close Completely Remove group Completely Remove group? Enter groupname First and second password must be identical Group Password No group Recoverable: No group Selected: Notice!
        A group can only be recovered if the group
        has not been completely removed! Notice!
        All of the users in this group will be 
        logged, unless completely remove
        is checked. Password Password Management Passwords Recover Recover group Remove Remove group Repair group Select Group The groupname cannot contain spaces The password cannot contain spaces There are no records of a recoverable group. Users Users to Group You MUST be root to use this application! You must choose a group You need to enter a groupname You need to enter a password group Password group Removal password password again Project-Id-Version: antix-development
POT-Creation-Date: 2013-07-06 17:18+EEST
PO-Revision-Date: 2018-09-23 22:12+0300
Last-Translator: cmenemrs <cmenemrs@gmail.com>
Language-Team: Portuguese (Brazil) (http://www.transifex.com/anticapitalista/antix-development/language/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 1.8.11
X-Poedit-SourceCharset: UTF-8
 Adicionar grupo Adicionar grupo Adicionar grupos Aplicar Escolher grupo Encerrar Remoção completa de grupo Remoção completa de grupo? Introduzir nomedegrupo A primeira e a segunda entradas de senha têm que ser iguais Senha de grupo Nenhum grupo recuperável: Nenhum grupo seleccionado: Alerta!
Um grupo só pode ser recuperado se não
tiver sido completamente removido! Atenção!
Todos os utilizadores deste grupo
serão registados, a menos que seja
selecionado remover completamente. Senha Gestão de senhas Senhas  Recuperar Recuperar grupo Remover Remover grupo Reparar grupo Seleccionar grupo O nomedegrupo não pode conter espaços A senha não pode conter espaços Não há registos de qualquer grupo recuperável Utilizadores Utilizadores para grupo TEM de ser root para executar esta aplicação! Tem que escolher um grupo Tem que introduzir um nomedegrupo Tem que introduzir uma senha Senha de grupo Remoção de grupo senha novamente senha 