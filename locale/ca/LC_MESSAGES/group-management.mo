��    &      L  5   |      P  	   Q  	   [  
   e     p     v     �     �     �     �  +   �     �            c   .  t   �            	   $     .     6     D     K     X     e  #   r  "   �  ,   �     �     �  )   �     %     =     [     x     �     �     �  �  �     �     �     �     �     �     �     �     �     	  -   	     M	     b	     	  b   �	  ~   �	     z
     �
     �
  	   �
     �
     �
     �
     �
     �
  &     %   *  +   P     |     �  5   �     �  "   �  #        *     @     T     a           !                           	   
                                               "                                 &                           #      %                    $    Add Group Add group Add groups Apply Choose group Close Completely Remove group Completely Remove group? Enter groupname First and second password must be identical Group Password No group Recoverable: No group Selected: Notice!
        A group can only be recovered if the group
        has not been completely removed! Notice!
        All of the users in this group will be 
        logged, unless completely remove
        is checked. Password Password Management Passwords Recover Recover group Remove Remove group Repair group Select Group The groupname cannot contain spaces The password cannot contain spaces There are no records of a recoverable group. Users Users to Group You MUST be root to use this application! You must choose a group You need to enter a groupname You need to enter a password group Password group Removal password password again Project-Id-Version: antix-development
POT-Creation-Date: 2013-07-06 17:18+EEST
PO-Revision-Date: 2018-09-23 22:08+0300
Last-Translator: Eduard Selma <selma@tinet.cat>
Language-Team: Catalan (http://www.transifex.com/anticapitalista/antix-development/language/ca/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ca
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
X-Poedit-SourceCharset: UTF-8
 Afegir grup Afegeix grup  Afegeix grups  Aplica Trieu un grup  Tanca Elimina el grup completament  Eliminar el grup completament?  introduïu el nom de grup  Les dues contrasenyes han de ser idèntiques  Contrasenya de grup  No es pot recuperar el grup: No s'ha triat cap grup:  Atenció!
        Només es pot recuperar un grup si aquest
        no s'ha eliminat completament! Atenció!
        Tots els usuaris d'aquest grup 
        estaran registrats, llevat que trieu
        l'eliminació completa. Contrasenya Administració de contrasenyes  Contrasenyes Recupera  Recupera un grup  Elimina  Elimina el grup Repara grup  Trieu el Grup El nom de grup no pot contenir espais  La contrasenya no pot contenir espais No hi ha registres de cap grup recuperable. Usuaris  Usuaris del grup  Cal ser usuari principal per usar aquesta aplicació! Cal que trieu un grup Cal que introduïu un nom de grup  Cal que introduïu una contrasenya  Contrasenya del grup  Eliminació de grup contrasenya  un altre cop la contrasenya  