��    &      L  5   |      P  	   Q  	   [  
   e     p     v     �     �     �     �  +   �     �            c   .  t   �            	   $     .     6     D     K     X     e  #   r  "   �  ,   �     �     �  )   �     %     =     [     x     �     �     �  �  �     }     �     �     �     �     �  *   �  +   *	  !   V	  b   x	  %   �	  1   
  *   3
  v   ^
  �   �
     ~  *   �     �     �     �               8     O  ;   f  >   �  :   �       #   /  Y   S  3   �  A   �  D   #  %   h     �     �  1   �           !                           	   
                                               "                                 &                           #      %                    $    Add Group Add group Add groups Apply Choose group Close Completely Remove group Completely Remove group? Enter groupname First and second password must be identical Group Password No group Recoverable: No group Selected: Notice!
        A group can only be recovered if the group
        has not been completely removed! Notice!
        All of the users in this group will be 
        logged, unless completely remove
        is checked. Password Password Management Passwords Recover Recover group Remove Remove group Repair group Select Group The groupname cannot contain spaces The password cannot contain spaces There are no records of a recoverable group. Users Users to Group You MUST be root to use this application! You must choose a group You need to enter a groupname You need to enter a password group Password group Removal password password again Project-Id-Version: antix-development
POT-Creation-Date: 2013-07-06 17:18+EEST
PO-Revision-Date: 2018-09-23 22:07+0300
Last-Translator: samson <sambelet@yahoo.com>
Language-Team: Amharic (http://www.transifex.com/anticapitalista/antix-development/language/am/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: am
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 1.8.11
X-Poedit-SourceCharset: UTF-8
 ቡድን መጨመሪያ ቡድን መጨመሪያ ቡድን መጨመሪያ መፈጸሚያ ቡድን ይምረጡ መዝጊያ በ ሙሉ ቡድኑን ማሴስወገጃ በ ሙሉ ቡድኑን ላስወግደው? የ ቡድን ስም ያስገቡ የ መጀመሪያው እና ሁለተኛው የ መግቢያ ቃል መመሳሰል አለበት የ ቡድን የ መግቢያ ቃል ምንም ቡድን ማዳን አልተቻለም: ምንም ቡድን አልተመረጠም: ማስታወቂያ!
        ቡድን ማዳን የሚቻለው ቡድኑ
        በ ሙሉ ካልተወገደ ነው ማስታወቂያ!
        ሁሉም በዚህ ቡድን ውስጥ ያሉ ይገባሉ 
        በ ሙሉ ማስወገጃ ላይ ምልክት ካልተደረገበት የ መግቢያ ቃል የ መግቢያ ቃል አስተዳዳሪ የ መግቢያ ቃል መልሶ ማግኛ ቡድን መልሶ ማግኛ ማስወገጃ ቡድን ማሴስወገጃ ቡድን መጠገኛ ቡድን ይምረጡ የ ቡድን ስም ክፍተት መያዝ የለበትም የ መግቢያ ቃል ክፍተት መያዝ የለበትም የሚድን ቡን ምንም መረጃ አልተገኘም ተጠቃሚዎች ተጠቃሚዎች ወደ ቡድን ይህን መተግበሪያ ለ መጠቀም እርስዎ root መሆን አለብዎት እርስዎ ቡድን መምረጥ አለብዎት እርስዎ የ ቡድን ስም ማስገባት አለብዎት እርስዎ የ መግቢያ ቃል ማስገባት አለብዎት የ ቡድን የ መግቢያ ቃል ቡድን ማስወገጃ የ መግቢያ ቃል የ መግቢያ ቃል እንደገና ይጻፉ 