��    &      L  5   |      P  	   Q  	   [  
   e     p     v     �     �     �     �  +   �     �            c   .  t   �            	   $     .     6     D     K     X     e  #   r  "   �  ,   �     �     �  )   �     %     =     [     x     �     �     �  p  �     	     <	     Z	     x	     �	     �	  .   �	  /   �	  "   
  L   :
     �
  E   �
  !   �
  �   	  �   �     <  %   I     o     |  %   �     �     �  %   �       E   *  >   p  U   �       %     t   D  -   �  2   �  +        F     `     ~     �           !                           	   
                                               "                                 &                           #      %                    $    Add Group Add group Add groups Apply Choose group Close Completely Remove group Completely Remove group? Enter groupname First and second password must be identical Group Password No group Recoverable: No group Selected: Notice!
        A group can only be recovered if the group
        has not been completely removed! Notice!
        All of the users in this group will be 
        logged, unless completely remove
        is checked. Password Password Management Passwords Recover Recover group Remove Remove group Repair group Select Group The groupname cannot contain spaces The password cannot contain spaces There are no records of a recoverable group. Users Users to Group You MUST be root to use this application! You must choose a group You need to enter a groupname You need to enter a password group Password group Removal password password again Project-Id-Version: antix-development
POT-Creation-Date: 2013-07-06 17:18+EEST
PO-Revision-Date: 2018-09-23 22:13+0300
Last-Translator: Вячеслав Волошин <vol_vel@mail.ru>
Language-Team: Russian (http://www.transifex.com/anticapitalista/antix-development/language/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
X-Generator: Poedit 1.8.11
X-Poedit-SourceCharset: UTF-8
 Добавить группу Добавить группу Добавить группы Применить Выберите группу Закрыть Полностью удалить группу Полностью удалить группу? Введите имя группы Введенные пароли должны быть одинаковыми Пароль группы Не выбрана группа для восстановления: Группа не выбрана: Внимание!
Группа может быть восстановлена,
только если не была удалена полностью! Внимание!
Все пользователи останутся
в этой группе, если не отмечено
полное удаление. Пароль Управление паролями Пароли Восстановить Восстановить группу Удалить Удалить группу Восстановить группу Выберите группу Имя группы не может содержать пробелы Пароль не может содержать пробелы Отсутствует запись группы для восстановления. Пользователи Пользователи группы Вы должны выполнять это приложение от имени суперпользователя! Вы должны выбрать группу Вы должны ввести имя группы Вы должны ввести пароль Пароль группы Удаление группы пароль пароль еще раз 